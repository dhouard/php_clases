<?php


/**
 * Description of Base
 *
 * @author dhouard
 */
class Base {
    
    static function trackDebug($sql, $debug) {
        if ($debug == 'screen') {
            echo $sql;
        }
        else {
            if ($debug == 'session') {
                $session = Session::getSession();
                $session->set('debug', $sql);
            }
        }
    }
    
    static function replacePlaceholder($data, $delimiters, $code) {
        
        $result = false;
        
        $phpVersion = explode('.', phpversion());
        $majorVersion = $phpVersion[0];
        $minorVersion = $phpVersion[1];
        
        if (strlen($delimiters) % 2 == 0) {
            $startDelimiter = substr($delimiters, 0, strlen($delimiters) / 2);
            $endDelimiter = substr($delimiters, -1 * strlen($delimiters) / 2);
        }
        else {
            $startDelimiter = $delimiters;
            $endDelimiter = $delimiters;
        }
        
        if (!empty($data)) {
            foreach ($data as &$row) {
                $row[0] = $startDelimiter.$row[0].$endDelimiter;
            }
            if (($majorVersion > 5) || ($majorVersion == 5 && $minorVersion >= 5)) {
                $result = preg_replace(array_column($data, 0), array_column($data, 1), $code);
            }
            else {
                $result = preg_replace(self::array_col($data, 0), self::array_col($data, 1), $code);
            }
        }
        
        return $result;
        
    }
    
    static public function array_col($array, $column) {
        
        $arrayAux = array();
        
        foreach ($array as $row) {
            $arrayAux[] = $row[$column];
        }
        
        return $arrayAux;
        
    }
    
}
